/**
 * Класс, имитирующий работу светофора, имеющий 3
 * состояния интерфейса: красный, желтый, зеленый.
 *
 * @author А.В. Рыжкин
 */

package com.example.andrey252.trafficlight;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button redButton;
    Button yellowButton;
    Button greenButton;

    // идентификатор цвета
    private int colorId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        redButton = (Button) findViewById(R.id.button_red);
        yellowButton = (Button) findViewById(R.id.button_yellow);
        greenButton = (Button) findViewById(R.id.button_green);

        redButton.setOnClickListener(this);
        yellowButton.setOnClickListener(this);
        greenButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_red:
                setBackgroundView(R.color.colorRed);
                break;
            case R.id.button_yellow:
                setBackgroundView(R.color.colorYellow);
                break;
            case R.id.button_green:
                setBackgroundView(R.color.colorGreen);
                break;
        }
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            colorId = savedInstanceState.getInt("color");
            setBackgroundView(colorId);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("color", colorId);
    }

    /**
     * Метод изменяет цвет экрана интерфейса
     * @param colorId идентификатор цвета
     */
    public void setBackgroundView(int colorId){
        this.colorId = colorId;
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.activity_main);
        linearLayout.setBackgroundColor(ContextCompat.getColor(this, colorId));
    }
}